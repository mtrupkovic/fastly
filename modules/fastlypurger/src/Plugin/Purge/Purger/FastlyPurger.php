<?php

namespace Drupal\fastlypurger\Plugin\Purge\Purger;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ConfigFactoryInterface;

use Drupal\fastly\Api;
use Drupal\fastly\EventSubscriber\SurrogateKeyGenerator;

use Drupal\purge\Plugin\Purge\Purger\PurgerBase;
use Drupal\purge\Plugin\Purge\Purger\PurgerInterface;
use Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Fastly purger.
 *
 * @PurgePurger(
 *   id = "fastly",
 *   label = @Translation("Fastly"),
 *   description = @Translation("Purger for Fastly."),
 *   types = {"tag"},
 *   multi_instance = FALSE,
 * )
 */
// @TODO: Verify above annotations and see if we want to add others.
class FastlyPurger extends PurgerBase implements PurgerInterface {

  /**
   * Cache tags invalidator implementation that invalidates Fastly.
   *
   * @var \Drupal\fastly\CacheTagsInvalidator
   */
  protected $invalidator;

    /**
     * Fastly API.
     *
     * @var \Drupal\fastly\Api
     */
    protected $fastlyApi;
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * Constructs a \Drupal\Component\Plugin\FastlyPurger.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The factory for configuration objects.
   * @throws \LogicException
   *   Thrown if $configuration['id'] is missing, see Purger\Service::createId.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->config = $config->get('faslty.settings');
      $this->fastlyApi = \Drupal::service('fastly.api');


  }

  /**
   * {@inheritdoc}
   */
  public function hasRuntimeMeasurement() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function routeTypeToMethod($type) {
    $methods = [
      'tag'  => 'invalidate'

    ];
    return isset($methods[$type]) ? $methods[$type] : 'invalidate';
  }

  /**
   * {@inheritdoc}
   */
  public function invalidate(array $invalidations) {
      $tags = [];

      foreach ($invalidations as $invalidation) {
          $invalidation->setState(InvalidationInterface::PROCESSING);
          $tags[] = $invalidation->getExpression();


      }

      try{

          if (in_array('config:core.extension', $tags)) {
              $this->fastlyApi->purgeAll();

          }
          foreach ($tags as $tag) {
              $this->fastlyApi->purgeKey($tag);
          }
          // Also invalidate the cache tags as hashes, to automatically also work for
          // responses that exceed the 16 KB header limit.
          $hashes = SurrogateKeyGenerator::cacheTagsToHashes($tags);
          foreach ($hashes as $hash) {
              $this->fastlyApi->purgeKey($hash);
          }

          foreach ($invalidations as $invalidation) {
              $invalidation->setState(InvalidationInterface::SUCCEEDED);


          }

      }

      catch (\Exception $e) {
          foreach ($invalidations as $invalidation) {
              $invalidation->setState(InvalidationInterface::FAILED);
          }

          // We only want to log a single watchdog error per request. This prevents
          // the log from being flooded.
          $this->logger->error($e->getMessage());
      }


  }



}
