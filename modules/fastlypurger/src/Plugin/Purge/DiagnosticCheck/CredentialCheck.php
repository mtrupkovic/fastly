<?php

namespace Drupal\fastlypurger\Plugin\Purge\DiagnosticCheck;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\purge\Plugin\Purge\DiagnosticCheck\DiagnosticCheckBase;
use Drupal\purge\Plugin\Purge\DiagnosticCheck\DiagnosticCheckInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Checks that the site is within Fastly's API key.

 * @PurgeDiagnosticCheck(
 *   id = "fastly_api_key_check",
 *   title = @Translation("Fastly - Api Credential Check."),
 *   description = @Translation("Checks Fastly's Api key."),
 *   dependent_queue_plugins = {},
 *   dependent_purger_plugins = {"fastly"}
 * )
 */
class CredentialCheck extends DiagnosticCheckBase implements DiagnosticCheckInterface {
    /**
     * The settings configuration.
     *
     * @var \Drupal\Core\Config\Config
     */
    protected $config;

    /**
    /**
     * {@inheritdoc}
     */
    public function __construct( array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config) {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
        $this->config = $config->get('fastly.settings');
    }
    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container , array $configuration, $plugin_id, $plugin_definition) {
        return new static(

            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('config.factory')
        );
    }
    /**
     * {@inheritdoc}
     */
    public function run() {
        $has_valid_credentials = $this->config->get('api_key');

        if (!$has_valid_credentials) {
            $this->recommendation = $this->t("Invalid Api credentials.");
            return SELF::SEVERITY_ERROR;
        }

        $this->recommendation = $this->t('Valid Api credentials detected.');
        return SELF::SEVERITY_OK;
    }

}
