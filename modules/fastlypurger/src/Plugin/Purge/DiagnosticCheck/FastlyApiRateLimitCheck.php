<?php

namespace Drupal\fastlypurger\Plugin\Purge\DiagnosticCheck;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\purge\Plugin\Purge\DiagnosticCheck\DiagnosticCheckBase;
use Drupal\purge\Plugin\Purge\DiagnosticCheck\DiagnosticCheckInterface;
/**
 * Checks that the site is within Fastly's API rate limits.
 *
 * Surrogate key and URL purge requests are also not limited,
 * but purge all requests count against the per-hour rate limit.
 *
 * @see https://docs.fastly.com/api/#rate-limiting
 *
 * @PurgeDiagnosticCheck(
 *   id = "fastly_api_rate_limit_check",
 *   title = @Translation("Fastly - Api Rate limit check."),
 *   description = @Translation("Checks that the site is not violating Fastly's overall Api rate limit."),
 *   dependent_queue_plugins = {},
 *   dependent_purger_plugins = {"fastly"}
 * )
 */
class FastlyApiRateLimitCheck extends DiagnosticCheckBase implements DiagnosticCheckInterface {

    /**
     * {@inheritdoc}
     */
    public function __construct( array $configuration, $plugin_id, $plugin_definition) {
        parent::__construct($configuration, $plugin_id, $plugin_definition);

    }
    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(

            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('config.factory')
        );
    }
    /**
     * {@inheritdoc}
     */
  public function run() {
    // TODO: Implement run() method.
        $this->recommendation = $this->t(" Api Rate limit 1000");
          return SELF::SEVERITY_OK;

  }
}
